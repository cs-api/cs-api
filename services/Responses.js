module.exports = new class Responses
{
    constructor()
    {
        this.CODE_SUCCESS = 1;
        this.CODE_ERROR = 2;

        this.DESCRIPTION_OK = 'OK';
        this.DESCRIPTION_ERROR = 'An error has occurred.';

    }

    errorResponsePromise(description = new Responses().DESCRIPTION_ERROR, code = new Responses().CODE_ERROR, stackTrace)
    {
        return new Promise(function (resolve, reject)
        {
            var response = new Responses();
            response.status =
                {
                    code: code,
                    description: description,
                    stackTrace: stackTrace
                }

            reject(response);
        });
    }

    promiseRejectionResponse(reason)
    {
        var response = reason;
        if (response.hasOwnProperty("status") === false)
        {
            response = new Responses();
            response.status =
                {
                    code: this.CODE_ERROR,
                    description: reason.toString(),
                    stackTrace: ''
                }
        }
        return response;
    }

    successResponse()
    {
        var response = new Responses();
        response.status =
            {
                code: this.CODE_SUCCESS,
                description: response.DESCRIPTION_OK,
                stackTrace: ''
            }
        return response;
    }

    errorResponse()
    {
        var response = new Responses();
        response.status =
            {
                code: this.CODE_ERROR,
                description: this.DESCRIPTION_ERROR,
                stackTrace: ''
            }
        return response;
    }
}