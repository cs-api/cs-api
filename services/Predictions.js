import Responses from './Responses';
import Models from './Models';
import { createCanvas, loadImage } from 'canvas';
import * as tf from '@tensorflow/tfjs-node-gpu';

const fs = require('fs');
const util = require('util');
const readdir = util.promisify(fs.readdir);
const path = require('path');
const uuidv4 = require('uuid').v4;
var vega = require('vega');
var axios = require('axios');

module.exports = new class Predictions {
    constructor(props) {

    }

    async train(clean) {
        var response = Responses.errorResponse();

        try {
            fs.unlinkSync('model.json');
            fs.unlinkSync('weights.bin');

        }
        catch
        {

        }

        const model = Models.createModel();

        try {
            var images = [];
            var labels = [];

            const trainingFolder = 'wf-training-graphs';
            const dirs = await readdir();

            var classId = 0;
            for (var dirIdx = 0; dirIdx < dirs.length; dirIdx++) {
                const currentClassId = classId;
                //the name of the directory holding the images, will specify
                //the classname
                const dir = dirs[dirIdx];

                //add the files to the classifier
                const dirPath = path.join(trainingFolder, dir);
                const files = await readdir(dirPath)

                var count = 0;
                for (var fileIdx = 0; fileIdx < files.length; fileIdx++) {
                    const file = files[fileIdx];
                    const filePath = path.join(trainingFolder, dir, file);
                    //const addResposne = await this.addClassImage(model, filePath, currentClassId);
                    //console.log(addResposne.evalInfo);

                    //const img = await loadImage(filePath);
                    //const canvas = createCanvas(28, 28);
                    //const ctx = canvas.getContext('2d');
                    //ctx.drawImage(img, 0, 0, 28, 28);

                    //debug - save image to file so that we can validate
                    //const buffer = canvas.toBuffer('image/png');
                    //fs.writeFileSync(`./validation/${uuidv4()}.png`, buffer);

                    const imageBuffer = fs.readFileSync(filePath);
                    const tensor = tf.node.decodePng(imageBuffer, 1);
                    //const buffer = tensor.dataSync().buffer;
                    //fs.writeFileSync(`./validation/${uuidv4()}.png`, new Float32Array(buffer));

                    //const tensorExpanded = tensor.expandDims(0);

                    console.log(`START - adding image to class ${classId} - ${filePath}`);
                    labels.push(classId);
                    images.push(tensor);

                    count++;
                }

                classId++;
            }
        }
        catch (err) {
            if (err) {
                console.error("Could not list the directory.", err);
                process.exit(1);
            }
        }

        var tensorImages = tf.stack(images);
        var tensorLabels = tf.oneHot(tf.tensor1d(labels, 'int32'), 3);
        //tensorLabels.print(true);

        const batchSize = 10;
        const epochs = 10;
        const validationSplit = 0.15;

        await model.fit(tensorImages, tensorLabels,
            {
                epochs,
                batchSize,
                validationSplit
            });
        //const evalInfo = await model.evaluate(tensorImages, tensorLabels);
        //console.log(evalInfo);

        await model.save('file://');

        return response;
    }

    async getPredictions(path) {
        var response = Responses.errorResponse();

        console.log(`getPredictions for ${path}`);

        var model = await Models.getModel('file://model.json');

        if (model) {

            console.log(`decode image`);

            const imageBuffer = fs.readFileSync(path);
            const tensor = tf.node.decodePng(imageBuffer, 1);
            var tensorImage = tf.stack([tensor]);

            console.log(`predict`);

            const prediction = model.predict(tensorImage).arraySync();

            console.log(`predict results ${JSON.stringify(prediction)}`);

            response = Responses.successResponse();
            response.prediction = prediction;
        }

        return response;
    }

    async discover(symbols, classes)
    {
        var nowDate = new Date();
        var month = '' + (nowDate.getMonth() + 1);
        var day = '' + nowDate.getDate();
        var year = nowDate.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        
        const startDate = [year, month, day].join('-');

        for(var symbolIdx=0; symbolIdx<symbols.length; symbolIdx++)
        {
            let symbol = symbols[symbolIdx];
            const discoverResponse = await this.createDiscoveryImage('wf-discoveries', symbol, startDate);
            if(discoverResponse.status.code === Responses.CODE_SUCCESS)
            {
                let response = await this.getPredictions(discoverResponse.discoveryImagePath);
                console.log(`return from get predictions`);
                if(response.status.code === Responses.CODE_SUCCESS)
                {
                    var prediction = response.prediction;
                    if(prediction.length > 0)
                    {
                        var classIdx = 0;
                        var max = 0;
                        prediction[0].map((value, index) =>
                        {
                            if(value > max)
                            {
                                classIdx = index;
                                max = value;
                            }
                        });
                        const className = classes[classIdx];
                        console.log(`image classified as ${className}`);
                        fs.rename(discoverResponse.discoveryImagePath, `./wf-discoveries-classified/${className}/${discoverResponse.discoveryImageName}.png`, (err) =>
                        {
                            if(err)
                            {
                                console.log(`move failed: ${JSON.stringify(err)}`);
                            }
                            else
                            {
                                console.log(`move is successfull`);
                            }
                        });
                        //move the image to the class folder
                    }
                }
            }
        }
    }

    async createDiscoveryImage(folder, stockSymbol, discoveryDate) {
        var response = Responses.errorResponse();

        try {

            var date = new Date(discoveryDate);
            const unixMillis = Date.now();
            const discoveryDateString = this.getDateString(date);
            const startDate = new Date(date.setDate(date.getDate() - 30));
            const startDateString = this.getDateString(startDate);
            
            const axiosResponse = await axios.get(`https://api.polygon.io/v2/aggs/ticker/${stockSymbol}/range/5/minute/${startDateString}/${discoveryDateString}?unadjusted=true&sort=desc&limit=5880&apiKey=jN3DiQUs0IpzKQ4kGQm02WjvUip69SxB`);
            
            console.log(startDateString,discoveryDateString);
            
            console.log(`${axiosResponse.data.queryCount} => ${axiosResponse.data.count}`);

            var data = {
                name: "table",
                values: []
            };

            if(axiosResponse.data.results.length >= 1000)
            {
                const count = axiosResponse.data.results.length; //1000 5 minute bars
                var offset = 0;
                ///ensure that counts are always the same. Trading hours may differ???
                for (var x = 0; x < count; x++) {
                    const y = (axiosResponse.data.results[offset].h + axiosResponse.data.results[offset].l) / 2;
                    const v = axiosResponse.data.results[offset].v;
                    const vw = axiosResponse.data.results[offset].vw;
                    const t = axiosResponse.data.results[offset].t;

                    //console.log(x, unixMillis, t);
                    offset++;

                    if(unixMillis > t)
                    {
                        data.values.push({x:data.values.length, y, v, vw });
                        //console.log(offset, y);
                       
                        if (data.values.length >= 1000) 
                        {
                            break;
                        }
                    } 
                }
                data.values.sort((x1, x2) =>
                {
                    if (x1 > x2) 
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }
                });
                const name = `${stockSymbol}-${discoveryDateString}-${unixMillis}`;
                const createChartImageResponse = await this.createChartImage(folder, name, data);

                if(createChartImageResponse.status.code === Responses.CODE_SUCCESS)
                {
                    response = Responses.successResponse();
                    response.discoveryImagePath = `${folder}/${name}.png`;
                    response.discoveryImageFolder = folder;
                    response.discoveryImageName = name;
                }
                else
                {
                    response = this.createChartImage;
                }
            }
            else
            {
                //log and gen error
            }

            //console.log(response);
        } catch (error) {
            response.status.description = JSON.stringify(error);
            console.error(error);
        }

        return response;
    }

    async createChartImage(folder, filename, data) {

        return new Promise((resolve) => 
        {
            var response = Responses.errorResponse();

            console.log(`createChartImage folder: ${folder}, filename: ${filename}, length: ${data.values.length}`);

            const charData =
            {
                "$schema": "https://vega.github.io/schema/vega/v3.0.json",
                "width": 512,
                "height": 512,
                "padding": 0,
                "background": "#ffffff",
                "data": data,
                "marks": [
                    {
                        "type": "group",
                        "name": "price",
                        "encode": {
                            "enter": {
                                "x": { "value": 0 },
                                "y": { "value": 0 },
                                "height": { "value": 256 },
                                "width": { "value": 512 }
                            }
                        },
                        "scales": [
                            {
                                "name": "x",
                                "type": "point",
                                "range": "width",
                                "domain": {
                                    "data": "table",
                                    "field": "x"
                                },
                                "zero": false
                            },
                            {
                                "name": "y",
                                "type": "linear",
                                "range": [0, 256],
                                "domain": {
                                    "data": "table",
                                    "field": "y"
                                },
                                "zero": false
                            }
                        ],
                        "marks": [
                            {
                                "type": "group",
                                "encode": {
                                    "enter": {
                                        "height": { "field": { "group": "height" } },
                                        "width": { "field": { "group": "width" } },
                                        "clip": { "value": true }
                                    }
                                },
                                marks:
                                    [
                                        {
                                            "name": "priceData",
                                            "type": "line",
                                            "from": {
                                                "data": "table"
                                            },
                                            "encode": {
                                                "enter": {
                                                    "x": {
                                                        "scale": "x",
                                                        "field": "x"
                                                    },
                                                    "y": {
                                                        "scale": "y",
                                                        "field": "y"
                                                    },
                                                    "stroke": {
                                                        "value": "#000000"
                                                    },
                                                    "strokeWidth": {
                                                        "value": 1
                                                    }
                                                }
                                            }
                                        }
                                    ]
                            }
                        ]
                    },
                    {
                        "type": "group",
                        "name": "volumeWeightedPrice",
                        "encode": {
                            "enter": {
                                "x": { "value": 0 },
                                "y": { "value": 256 },
                                "height": { "value": 128 },
                                "width": { "value": 512 }
                            }
                        },
                        "scales": [
                            {
                                "name": "x",
                                "type": "point",
                                "range": "width",
                                "domain": {
                                    "data": "table",
                                    "field": "x"
                                },
                                "zero": false
                            },
                            {
                                "name": "vw",
                                "type": "linear",
                                "range": [0, 128],
                                "domain": {
                                    "data": "table",
                                    "field": "vw"
                                },
                                "zero": false
                            }
                        ],
                        "marks": [
                            {
                                "type": "group",
                                "encode": {
                                    "enter": {
                                        "height": { "field": { "group": "height" } },
                                        "width": { "field": { "group": "width" } },
                                        "clip": { "value": true }
                                    }
                                },
                                marks:
                                    [
                                        {
                                            "name": "volumeWeightedPriceData",
                                            "type": "line",
                                            "from": {
                                                "data": "table"
                                            },
                                            "encode": {
                                                "enter": {
                                                    "x": {
                                                        "scale": "x",
                                                        "field": "x"
                                                    },
                                                    "y": {
                                                        "scale": "vw",
                                                        "field": "vw"
                                                    },
                                                    "stroke": {
                                                        "value": "#ff0000"
                                                    },
                                                    "strokeWidth": {
                                                        "value": 1
                                                    }
                                                }
                                            }
                                        }
                                    ]
                            }
                        ]
                    },
                    {
                        "type": "group",
                        "name": "volume",
                        "encode": {
                            "enter": {
                                "x": { "value": 0 },
                                "y": { "value": 384 },
                                "height": { "value": 128 },
                                "width": { "value": 512 }
                            }
                        },
                        "scales": [
                            {
                                "name": "x",
                                "type": "point",
                                "range": "width",
                                "domain": {
                                    "data": "table",
                                    "field": "x"
                                },
                                "zero": false
                            },
                            {
                                "name": "v",
                                "type": "linear",
                                "range": [0, 128],
                                "domain": {
                                    "data": "table",
                                    "field": "v"
                                },
                                "zero": false
                            }
                        ],
                        "marks": [
                            {
                                "type": "group",
                                "encode": {
                                    "enter": {
                                        "height": { "field": { "group": "height" } },
                                        "width": { "field": { "group": "width" } },
                                        "clip": { "value": true }
                                    }
                                },
                                marks:
                                    [
                                        {
                                            "name": "volumeData",
                                            "type": "line",
                                            "from": {
                                                "data": "table"
                                            },
                                            "encode": {
                                                "enter": {
                                                    "x": {
                                                        "scale": "x",
                                                        "field": "x"
                                                    },
                                                    "y": {
                                                        "scale": "v",
                                                        "field": "v"
                                                    },
                                                    "stroke": {
                                                        "value": "#00ff00"
                                                    },
                                                    "strokeWidth": {
                                                        "value": 1
                                                    }
                                                }
                                            }
                                        }
                                    ]
                            }
                        ]
                    }
                ]
            };

            // create a new view instance for a given Vega JSON spec
            var view = new vega
                .View(vega.parse(charData))
                .renderer('none')
                .initialize();

            // generate static PNG file from chart
            view
                .toCanvas()
                .then(async (canvas) => {
                    // process node-canvas instance for example, generate a PNG stream to write var
                    // stream = canvas.createPNGStream();
                    console.log(`WRITE createChartImage folder: ${folder}, filename: ${filename}, length: ${data.values.length}`);

                    //always force 128x128
                    const img = await loadImage(canvas.toBuffer());
                    const scaledCanvas = createCanvas(512, 512);
                    const ctx = scaledCanvas.getContext('2d');
                    ctx.drawImage(img, 0, 0, 512, 512);
                    fs.writeFileSync(`${folder}/${filename}.png`, scaledCanvas.toBuffer())

                    response = Responses.successResponse();
                    response.imagePath = `${folder}/${filename}.png`;
                    resolve(response);
                })
                .catch((err) => {
                    console.log("Error writing PNG to file:");
                    console.error(err);
                    response.description = JSON.stringify(err);
                    resolve(response);
                });
        });
    }

    getDateString(date)
    {
        var month = '' + (date.getMonth() + 1);
        var day = '' + date.getDate();
        var year = date.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        const dateString = [year, month, day].join('-');

        return dateString;
    }
}