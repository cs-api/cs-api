const app = require('../app');
const express = require('express');
const router = express.Router();

const Responses = require('../services/Responses');
const predictionsService = require('./../services/Predictions');

var uuidV4 = require('uuid/v4');

router.use((req, res, next) =>
{
    next();
});

router.post("/train", async (req, res, next) =>
{
    console.log(`train the model`);
    try
    {
        const clean = req.body.clean;
        const response = await predictionsService.train(clean);
        res.send(response);
    }
    catch(reason)
    {
        console.log(`error reason ${reason}`);
        var response = Responses.errorResponse();
        response.status.description = reason;
        response.status.stackTrace = new Error().stack;
        res.send(response);
    }
});

router.post("/get", async (req, res, next) =>
{
    console.log(`getting predictions`);
    try
    {
        const imageURL = req.body.imageURL;

        const response = await predictionsService.getPredictions(imageURL);
        res.send(response);
    }
    catch(reason)
    {
        console.log(`error reason ${reason}`);
        var response = Responses.errorResponse();
        response.status.description = reason;
        response.status.stackTrace = new Error().stack;
        res.send(response);
    }
});

router.post("/create/training/image", async (req, res, next) =>
{
    console.log(`create training image`);
    try
    {
        const stockSymbol = req.body.stockSymbol;
        const discoveryDate = req.body.discoveryDate;
        const className = req.body.className;

        const response = await predictionsService.createDiscoveryImage(`wf-training-graphs/${className}`, stockSymbol, discoveryDate);
        res.send(response);
    }
    catch(reason)
    {
        console.log(`error reason ${reason}`);
        var response = Responses.errorResponse();
        response.status.description = reason;
        response.status.stackTrace = new Error().stack;
        res.send(response);
    }
});

module.exports = router;