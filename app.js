console.log('starting candlestick service');

let express = require('express');
//export the app - for instance to be accessible to other dependent modules
let app = module.exports = express();
var server = require('http').Server(app);
var bodyParser = require('body-parser');
var cors = require('cors');
var uuidV4 = require('uuid/v4');
var config = require('./config');
const preditionsService = require("./services/Predictions");

app.use('/ui', express.static('public'))

app.use(bodyParser.json());
app.use(cors());

const port = process.env.port || 4000;
console.log('starting candlestick listener');
server.listen(port, () => console.log('server listening on port ' + port));

if(config.monitor)
{
    preditionsService.discover(config.symbols, config.classes);
    setInterval(() =>
    {
        preditionsService.discover(config.symbols, config.classes);
    },config.interval);
}
else
{
    //web api routers
    var counterRouter = require('./api-router/predictions-router');
    app.use('/candlesticks/predictions', counterRouter);
}